package com.example.trivial;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Shader;

public class prefques {

    private static final String mypre = "message";
    private static final String keys = "mssg";

    //to save data
    public static void savequestionindex(Context context, int qustionprefindex) {
        SharedPreferences pref = context.getSharedPreferences(mypre, Context.MODE_PRIVATE);
        SharedPreferences.Editor edite = pref.edit();
        edite.putInt(keys,qustionprefindex);
        edite.apply();

    }

    public static int loadsorefrompref(Context context) {
        SharedPreferences pref = context.getSharedPreferences(mypre, Context.MODE_PRIVATE);
        return pref.getInt(keys, 0);
    }

}


