package com.example.trivial.data;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.trivial.controller.AppController;
import com.example.trivial.model.Question;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    //making object of arraylist of question type.
    ArrayList<Question> questionArrList = new ArrayList<>();
    String url = "https://raw.githubusercontent.com/curiousily/simple-quiz/master/script/statements-data.json";

    public List<Question> getQuestions(final answerlistasync Callback) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {

                    try {
                        //creating question object
                        Question question = new Question(response.getJSONArray(i).getString(0), response.getJSONArray(i).getBoolean(1));

                        //add questions to arraylist/list
                        questionArrList.add(question);
                        Log.d("Hello", "getQues" + questionArrList);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (Callback != null) {
                    Callback.processfinished(questionArrList);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", "error");
            }
        });

        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
        return questionArrList;
    }


}
