package com.example.trivial.data;

import com.example.trivial.model.Question;

import java.util.ArrayList;

public interface answerlistasync {
    //passing an array list of question object in processfinished
    void processfinished(ArrayList<Question> questionlists);
   // processfinished dont have a body here, which means whoever implements this arraylist async response i.e answer list asynch will have to receive this questionas.
}
