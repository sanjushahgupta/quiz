package com.example.trivial;


import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.SharedPreferences;
import android.database.DatabaseUtils;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.trivial.data.Repository;
import com.example.trivial.data.answerlistasync;
import com.example.trivial.databinding.ActivityMainBinding;
import com.example.trivial.model.Question;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private int currentQuestionIndex;
    private int scoreIndex;
    List<Question> questions;
    private int notallowdoubleclick = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scoreIndex = prefconf.loadsorefrompref(this);
        currentQuestionIndex = prefques.loadsorefrompref(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        // binding.questionTextView.setText(questionlists.get(currentQuestionIndex).getAnswer());


        questions = new Repository().getQuestions(questionlists -> {

                    binding.questionTextView.setText(questionlists.get(currentQuestionIndex).getAnswer());
                    updateCounter((ArrayList<Question>) questions);

                }

        );

        binding.Scores.setText(String.format(getString(R.string.scoresyindex), scoreIndex));


        binding.buttonNext.setOnClickListener(view -> {
            notallowdoubleclick = 0;
            binding.buttonFalse.setEnabled(true);
            binding.buttonTrue.setEnabled(true);
            currentQuestionIndex = (currentQuestionIndex + 1) % questions.size();
            updateQuestion();
        });

        binding.buttonBack.setOnClickListener(view -> {
            if (currentQuestionIndex > 0) {
                currentQuestionIndex = (currentQuestionIndex - 1) % questions.size();
                updateQuestion();
                binding.buttonTrue.setEnabled(false);
                binding.buttonFalse.setEnabled(false);
                String enablemsg = "You have already given answer for this question so you are not allowed to choose true or false ";
                Snackbar.make(binding.cardView, enablemsg, Snackbar.LENGTH_SHORT).show();
            }
        });

        binding.buttonFalse.setOnClickListener(View -> {

            checkAnswer(false);
            updateQuestion();


        });

        binding.buttonTrue.setOnClickListener(View -> {
            checkAnswer(true);

            updateQuestion();

        });

        binding.buttonStart.setOnClickListener(View -> {
            startQuestion();
            notallowdoubleclick = 0;

            binding.buttonTrue.setEnabled(true);
            binding.buttonFalse.setEnabled(true);


        });


    }

    private void checkAnswer(boolean userChooseCorrect) {
        boolean answer = questions.get(currentQuestionIndex).isAnswerTrue();
        int messageId;
        String enablems = "you are not allowed to give answer twice. ";
        if (userChooseCorrect == answer) {

            notallowdoubleclick++;
            if (notallowdoubleclick > 1) {

                Snackbar.make(binding.cardView, enablems, Snackbar.LENGTH_SHORT).show();

            } else {
                addpoints();
                anim();
                messageId = R.string.correctanswer;
                Snackbar.make(binding.cardView, messageId, Snackbar.LENGTH_SHORT).show();

            }


        } else {
            anim1();

            notallowdoubleclick++;
            if (notallowdoubleclick > 1) {

                Snackbar.make(binding.cardView, enablems, Snackbar.LENGTH_SHORT).show();

            } else {
                messageId = R.string.Incorrect;
                Snackbar.make(binding.cardView, messageId, Snackbar.LENGTH_SHORT).show();
            }
        }


    }

    private void updateQuestion() {


        prefques.savequestionindex(getApplicationContext(), currentQuestionIndex);
        binding.questionTextView.setText(questions.get(currentQuestionIndex).getAnswer());

        updateCounter((ArrayList<Question>) questions);


    }

    private void updateCounter(ArrayList<Question> questionlists) {

        prefques.savequestionindex(getApplicationContext(), currentQuestionIndex);

        binding.outofindex.setText(String.format(getString(R.string.questionindex), currentQuestionIndex, questionlists.size()));
    }


    private void startQuestion() {
        currentQuestionIndex = 0;
        updateCounter((ArrayList<Question>) questions);
        String qu = questions.get(0).getAnswer();
        binding.questionTextView.setText(qu);
        binding.Scores.setText(String.format(getString(R.string.scoresyindex), 0));
        scoreIndex = 0;
        prefconf.savescore(getApplicationContext(), scoreIndex);

    }


    private void anim() {
        Animation shakes = AnimationUtils.loadAnimation(MainActivity.this, R.anim.anim);
        binding.cardView.setAnimation(shakes);

        shakes.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


                binding.questionTextView.setTextColor(Color.GREEN);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                binding.questionTextView.setTextColor(Color.WHITE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void anim1() {
        Animation shake = AnimationUtils.loadAnimation(MainActivity.this, R.anim.anim);
        binding.cardView.setAnimation(shake);

        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


                binding.questionTextView.setTextColor(Color.RED);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                binding.questionTextView.setTextColor(Color.WHITE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }


    private void addpoints() {

        scoreIndex++;


        prefconf.savescore(getApplicationContext(), scoreIndex);
        binding.Scores.setText(String.format(getString(R.string.scoresyindex), scoreIndex));
    }


}




